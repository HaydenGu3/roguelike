﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	public bool isPlayerTurn;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 50;
	public AudioClip gameOverSound;

	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject levelImage;
	private GameObject startImage;
	private Text levelText;
	private bool settingUpGame;
	private int secondsUntilLevelStart = 2;
	private int currentLevel = 1;
	public int charSelect;
	public Text healthText;
	public Text difficultyText;
	public Text mapText;
	public bool easy;
	public bool medium;
	public bool hard;
	public bool map1 = true;

	void Awake () {
		if (Instance != null && Instance != this) {
			DestroyImmediate(gameObject);
			return;
		}
		Instance = this;
		DontDestroyOnLoad (gameObject);
		DontDestroyOnLoad (healthText);
		boardController = GetComponent<BoardController> ();
		enemies = new List<Enemy> ();
	}

	void Start(){
		InitializeGame ();
	}




	public void CancelInvoke(){
		DisabledStartScreen ();
		levelImage = GameObject.Find("Level Image");
		levelText = GameObject.Find ("Level Text").GetComponent<Text> ();
		levelText.text = "Day " + currentLevel;
		levelImage.SetActive (true);
		enemies.Clear ();
		boardController.SetupLevel (currentLevel);
		Invoke ("DisabledLevelImage", secondsUntilLevelStart);
	}

	private void DisabledStartScreen(){
		startImage.SetActive (false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
	}

	private void InitializeGame(){

		settingUpGame = true;
		if (currentLevel == 1) {
			difficultyText = GameObject.Find ("Difficulty Text").GetComponent<Text> ();
			mapText = GameObject.Find ("Map").GetComponent<Text> ();
			startImage = GameObject.Find ("Start Image");
			startImage.SetActive (true);
		} else {
			startImage = GameObject.Find ("Start Image");
			startImage.SetActive (false);
			levelImage = GameObject.Find ("Level Image");
			levelText = GameObject.Find ("Level Text").GetComponent<Text> ();
			healthText = GameObject.Find ("Health Text").GetComponent<Text> ();
			levelText.text = "Day " + currentLevel;
			levelImage.SetActive (true);
			enemies.Clear ();
			boardController.SetupLevel (currentLevel);
			Invoke ("DisabledLevelImage", secondsUntilLevelStart);
		}
	}

	public void ChoosePlayer(){
		charSelect = 0;
		CancelInvoke();
	}
	
	public void ChooseKnight(){
		charSelect = 1;
		CancelInvoke();
	}
	


	private void DisabledLevelImage(){
		levelImage.SetActive (false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
	}



	private void OnLevelWasLoaded(int levelLoaded){
		currentLevel ++;
		InitializeGame ();
	}
	
	void Update () {
		if (isPlayerTurn || areEnemiesMoving || settingUpGame) {
			return;
		}

		StartCoroutine (MoveEnemies ());
	}

	private IEnumerator MoveEnemies(){
		areEnemiesMoving = true;

		yield return new WaitForSeconds(0.2f);

		foreach (Enemy enemy in enemies) {
			enemy.MoveEnemy();
			yield return new WaitForSeconds(enemy.moveTime);
		}

		areEnemiesMoving = false;
		isPlayerTurn = true;
	}

	public void AddEnemyToList(Enemy enemy){
		enemies.Add (enemy);
	}

	public void GameOver(){
		isPlayerTurn = false;
		SoundController.Instance.music.Stop();
		SoundController.Instance.PlaySingle(gameOverSound);

		if (currentLevel > 1) {
			levelText.text = "You starved after " + currentLevel + " days...";
		}
		else {
			levelText.text = "You starved after " + currentLevel + " day...";
		}

		levelImage.SetActive (true);
		enabled = false;

	}

	public void Easy(){
		easy = true;
		medium = false;
		hard = false;
		difficultyText.text = "Easy";

	}
	public void Medium(){
		easy = false;
		medium = true;
		hard = false;
		difficultyText.text = "Medium";
	}
	public void Hard(){
		easy = false;
		medium = false;
		hard = true;
		difficultyText.text = "Hard";
	}

	public void Map1(){
		map1 = true;
		mapText.text = "Map 1";
	}
	
	public void Map2(){
		map1 = false;
		mapText.text = "Map 2";
	}
}
